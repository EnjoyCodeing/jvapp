import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from '@react-navigation/native';
import FileDownload from "./src/page/fileDownload";
import Refresh from "./src/page/refresh";
import Nav from "./src/Nav";
import CursorField from "./src/page/cursorField";
import NumberKeyBoard from "./src/page/numberKeyBoard";
import ChargeIndicator from './src/page/chargeIndicator';
import Demo from './src/page/demo';

const Stack = createStackNavigator();

function RootStack() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Nav">
      <Stack.Screen name="Nav" component={Nav} />
      <Stack.Screen name="Demo" component={Demo} />
      <Stack.Screen name="ChargeIndicator" component={ChargeIndicator} />
      <Stack.Screen name="FileDownload" component={FileDownload} />
      <Stack.Screen name="NumberKeyBoard" component={NumberKeyBoard} />
      <Stack.Screen name="Refresh" component={Refresh} />
      <Stack.Screen name="CursorField" component={CursorField}/>
      </Stack.Navigator>
    </NavigationContainer>
  )
}

export default RootStack;