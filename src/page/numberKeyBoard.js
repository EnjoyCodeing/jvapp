import React from 'react';
import {
  SafeAreaView,
  FlatList,
  Dimensions,
  StyleSheet,
  View,
  Text,
  Image,
  TouchableOpacity
} from 'react-native';

class PinDot extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    let {numberOfDots, fillDot, style} = this.props;
    let items = [];
    for (let i = 0; i < numberOfDots; i++ ) {
      items.push({sel:i<fillDot, index:`${i}`})
    }
    return (
      <View style={[dotStyles.dotCon, style]}>
        {
          items.map(function (item) {
            return (
              <View style={dotStyles.dotItem} key={item.index}>
                <View style={
                  [dotStyles.dotSize,
                  dotStyles.emptyDot,
                  item.sel && dotStyles.fillDot]}>
                </View>
              </View>
            )
          })
        }
      </View>
    );
  }
}

const dotStyles = StyleSheet.create({
  dotSize:{
    width: 20,
    height: 20,
    borderRadius: 10,
  },
  dotItem:{
    width:40,
    height:30,
    justifyContent:'center',
    alignItems: 'center'
  },
  dotCon: {
    flexDirection: 'row',
    justifyContent: 'center'
  },
  fillDot: {
    backgroundColor: 'black',
  },
  emptyDot: {
    backgroundColor: 'white',
  }
})

const AcitonEmpty = "empty";
const ActionValue = "valeu";
const ActionDelete = "delete";

class GridCom extends React.Component {

  constructor(props) {
    super(props);
    let keyDatas = []
    for (let i = 1; i <= 12; i++) {
      let ind = `${i}`;
      let action = ActionValue;
      let value = `${i}`
      if (i === 10) {
        ind = " ";
        value = "";
        action = AcitonEmpty;
      } else if (i == 11) {
        ind = "0";
        value = "0"
        action = ActionValue;
      } else if (i == 12) {
        ind = "del";
        value = "";
        action = ActionDelete;
      }
      keyDatas.push({
        ind,
        action,
        value,
        key: `${i}`
      })
    }
    this.keyDatas = keyDatas;
  }

  render() {
    let { numInput } = this.props;
    return (
      <FlatList
      style={gridStyles.gridContainer}
      contentContainerStyle={gridStyles.gridContent}
        data={this.keyDatas}
        numColumns={3}
        renderItem={({ item }) => {
          return (
            <TouchableOpacity
              style={gridStyles.cell}
              onPress={() => {
                numInput(item);
              }}>
              <Text style={gridStyles.text}>{item.ind}</Text>
            </TouchableOpacity>
          )
        }}
      />
    );
  }
}

const dw = Dimensions.get('window').width;
const horMargin = 60;
const extra = 10;
const cellWH = (dw - horMargin - extra) / 3;

const gridStyles = StyleSheet.create({
  gridContainer:{
    marginLeft: horMargin,
    marginRight: horMargin
  },
  gridContent:{
    justifyContent:'center',
    alignItems: 'center'
  },
  cell: {
    width:cellWH,
    height:cellWH,
    justifyContent:'center',
    alignItems:'center'
  },
  text: {
    fontSize: 30,
    fontWeight: '400'
  }
});

class NumberKeyBoard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      selDot: 0,
      numberStr: ""
    }
    this.total = 4;
  }

  numberInputFunc = (item) => {
    let { numberStr } = this.state;
    if(item.action == ActionDelete) {
      if (numberStr > 0) {
        numberStr = numberStr.slice(0, -1);
      }
    } else if(item.action == ActionValue) {
      if (numberStr.length < this.total) {
        numberStr += item.value;
      }
    }
    console.log("numberStr: "+numberStr);
    this.setState({
      numberStr,
      selDot: numberStr.length
    })
  }

  render() {
    return(
      <>
      <SafeAreaView />
      <TouchableOpacity>
        <Image style={styles.img} source={{uri:"https://gitee.com/EnjoyCodeing/pic/raw/master/baconimg/close1.jpeg"}}/>
      </TouchableOpacity>
      <View style={styles.titleCon}>
        <Text style={styles.title}>设置PIN码</Text>
      </View>
      <View>
        <PinDot style={{marginTop:40}} numberOfDots={this.total} fillDot={this.state.selDot}/>
      </View>
        <GridCom numInput={this.numberInputFunc}/>
      </>
    )
  }
}

const styles = StyleSheet.create({
  img: {
    width: 30,
    height: 30,
    marginTop: 30,
    marginLeft: 30
  },
  titleCon: {
    marginTop:30,
  },
  title: {
    fontSize:25,
    fontWeight: '400',
    textAlign:'center'
  }
})


export default NumberKeyBoard;