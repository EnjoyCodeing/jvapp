import React from 'react';
import {StyleSheet, Dimensions} from 'react-native';
import {
  Surface,
  Shape,
  Group,
  Path,
  Transform,
  LinearGradient,
} from '@react-native-community/art';

function SomeShape() {
  let path = new Path().moveTo(0, 200).lineTo(200,200).close()
  return(
    <Surface width={'100%'} height={500} style={styles.surface}>
      <Shape 
      d={path} 
      strokeWidth={5} 
      stroke="#000000" 
      strokeDash={[10,20]}
      strokeCap='square'
      />
    </Surface>
  )
}

const styles = StyleSheet.create({
  surface: {
    backgroundColor: '#d39494',
  },
});

export default class Index extends React.Component {
  render() {
    return <SomeShape></SomeShape>
  }
}