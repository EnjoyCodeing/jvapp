import React, { Component } from "react";
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';

class FileDownload extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fullPath: ""
    }
  }
  startDownload = () => {
    RNFetchBlob.config({
      fileCache: true
    })
    .fetch('GET', 'http://localhost:8000/frp_0.34.1_darwin_amd64.tar.gz')
    .then((res) =>{
      this.setState({fullPath:res.path()});
      console.log('The file saved to ', res.path());
    })
  }

  render() {
    return(
      <>
      <View style={styles.contain}>
        <TouchableOpacity style={styles.btn} onPress={this.startDownload}>
          <Text>点击开始下载</Text>
        </TouchableOpacity>
        <View style={[styles.btn,{marginTop:20,height:80}]}>
          <Text>下载的问题路径是</Text>
        <Text>{this.state.fullPath}</Text>
      </View>
      </View>
      </>
    )
  }
}

const styles = StyleSheet.create({
  btn: {
    backgroundColor: 'skyblue',
    width: '90%',
    height: 44,
    justifyContent:'center'
  },
  contain: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center'
  }
})
export default FileDownload;