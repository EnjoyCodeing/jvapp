import React, { Component } from "react";
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native';
import {
  CodeField,
  Cursor,
} from 'react-native-confirmation-code-field';

class CursorField extends Component {
  constructor(props) {
    super(props);
    this.state = {
      vcodeTxt:""
    }
  }
  
  onVcodeChangeText = (vcodeTxt) => {
    this.setState({vcodeTxt})
  }

  render() {
    return(
      <>
      <View>
        <CodeField
          value={this.state.vcodeTxt}
          onChangeText={this.onVcodeChangeText}
          // onSubmitEditing={this.onVcodeCommit}
          cellCount={6}
          rootStyle={styles1.codeFieldRoot}
          keyboardType="number-pad"
          returnKeyType="done"
          renderCell={({ index, symbol, isFocused }) => (
            <Text
              key={index}
              style={[styles1.cell, isFocused && styles1.focusCell]}
            >
              {symbol || (isFocused ? <Cursor /> : null)}
            </Text>
          )}
        />
        </View>
      </>
    )
  }
}

const styles = StyleSheet.create({
  btn: {
    backgroundColor: 'skyblue',
    width: '90%',
    height: 44,
    justifyContent:'center'
  },
  contain: {
    flex:1,
    justifyContent: 'center',
    alignItems: 'center'
  }
});
const styles1 = StyleSheet.create({
  root: {flex: 1, padding: 20},
  title: {textAlign: 'center', fontSize: 30},
  codeFieldRoot: {marginTop: 70},
  cell: {
    width: 40,
    height: 40,
    lineHeight: 38,
    paddingBottom:2,
    fontSize: 24,
    // borderBottomWidth: 2,
    borderWidth:2,
    borderColor: 'lightgray',
    // borderBottomColor: 'red',
    textAlign: 'center',
    color: "#7d53ea"
  },
  focusCell: {
    borderColor: '#7d53ea',
  },
});

export default CursorField;