import React, { Component } from "react";
import { View, Text, FlatList, TouchableOpacity, StyleSheet } from 'react-native';

const dataSource = [
  {
    route: "CursorField",
    title: "数字输入框"
  },
  {
    route: "FileDownload",
    title: "文件下载"
  },
  {
    route: "Refresh",
    title: "下拉刷新-上拉加载"
  },
  {
    route: "NumberKeyBoard",
    title: "数字键盘"
  },
  {
    route: "ChargeIndicator",
    title: "充电进度条"
  },
  {
    route: "Demo",
    title: "Demo"
  }
]
class Nav extends Component {
  render() {
    let  { navigation } = this.props
    return(
      <>
      <FlatList 
      data={dataSource}
      renderItem={({item}) =>{
        return(
          <View >
            <TouchableOpacity 
            style={styles.cell}
            onPress={()=>{
              navigation.navigate(item.route)
            }}>
              <Text style={styles.text}>{item.title}</Text>
            </TouchableOpacity>
          </View>
        )
      }}
      keyExtractor={(item) => item.route}
      ItemSeparatorComponent={() =>{
        return <View style={{height:1, backgroundColor:'skyblue'}}></View>
      }}
      />
      </>
    )
  }
}

const styles = StyleSheet.create({
  text: {
    color: "black",
    marginLeft: 15,
    fontSize: 20
  },
  cell: {
    justifyContent: 'center',
    height: 44
  }
})

export default Nav;